import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { selectTheme } from '../store/selectors/settings.selectors';
import { AppState } from '../store/reducers';

@Component({
  selector: 'app-navbar',
  template: `
    <nav 
      *ngIf="(theme$ | async) as theme"
      class="navbar navbar-expand"
      [ngClass]="{
        'navbar-dark bg-dark text-white': theme === 'dark',
        'navbar-light bg-light': theme === 'light'
      }"
    >

      <a class="navbar-brand"
         routerLink="/login" routerLinkActive="active">NGRX</a>

      <div class="navbar-collapse collapse">
        <ul class="navbar-nav">

          <li class="nav-item"
              routerLink="/home" routerLinkActive="active">
            <a class="nav-link">Home</a>
          </li>

          <li class="nav-item"
              routerLink="/settings" routerLinkActive="active">
            <a class="nav-link">Settings</a>
          </li>

          <li class="nav-item"
              routerLink="/users" routerLinkActive="active">
            <a class="nav-link">Users</a>
          </li>
        </ul>
      </div>
    </nav>
  `
})
export class NavbarComponent {
  theme$: Observable<string> = this.store.pipe(select(selectTheme));

  constructor(private store: Store<AppState>) {}

}
