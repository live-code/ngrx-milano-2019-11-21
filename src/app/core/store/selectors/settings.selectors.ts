import { createSelector } from '@ngrx/store';
import { SettingsState } from '../reducers/settings.reducer';
import { AppState } from '../reducers';

// export const selectTheme = (state: AppState) => state.settings.theme;

export const selectSettings = (state: AppState) => state.settings;

export const selectTheme = createSelector(
  selectSettings,
  (state: SettingsState) => state.theme
);
