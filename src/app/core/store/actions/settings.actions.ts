import { createAction, props } from '@ngrx/store';

export const updateTheme = createAction(
  '[settings] update theme',
  props<{ theme: string }>()
);
