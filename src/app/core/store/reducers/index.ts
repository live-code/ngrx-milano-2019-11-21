import { ActionReducerMap } from '@ngrx/store';
import { settingsReducer, SettingsState } from './settings.reducer';


export interface AppState {
  settings: SettingsState;
  auth: { token: number };
}

export const reducers: ActionReducerMap<AppState> = {
  settings: settingsReducer,
  auth: () => ({ token: 123 }),
}
