import { createReducer, on } from '@ngrx/store';
import { updateTheme } from '../actions/settings.actions';

export interface SettingsState {
  theme: string;
  fontSize: number;
}

const INITIAL_STATE: SettingsState =  {
  theme: 'light',
  fontSize: 20
};

export const settingsReducer = createReducer(
  INITIAL_STATE,
  on(updateTheme, (state, action) => ({...state, theme: action.theme}))
);


