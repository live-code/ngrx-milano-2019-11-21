import { HttpClient } from '@angular/common/http';
import { User } from '../model/user';

const baseUrl = 'http://localhost:3000';

export class UsersService {
  constructor(private http: HttpClient) {}

  loadUsers() {
    return this.http.get<User[]>(`${baseUrl}/users`);
  }

  deleteUser(id: number) {
    return this.http.delete(`${baseUrl}/users/${id}`);
  }

  addUser(user: User) {
    return this.http.post<User>(`${baseUrl}/users`, user);
  }

  editUser(user: User) {
    return this.http.put<User>(`${baseUrl}/users/${user.id}`, user);
  }

}
