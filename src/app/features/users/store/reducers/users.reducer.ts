import { createReducer, on } from '@ngrx/store';
import { User } from '../../model/user';
import * as UsersActions from '../actions/users.actions';

export const initialState: User[] = [];

export const reducer = createReducer(
  initialState,
  on(UsersActions.loadUsersSuccess, (state, action) => [...action.users]),
  on(UsersActions.deleteUserSuccess, (state, action) => state.filter(u => u.id !== action.id)),
  on(UsersActions.addUserSuccess, (state, action) => [...state, action.user]),
  on(UsersActions.editUserSuccess, (state, action) => state.map(u => {
    return u.id === action.user.id ? {...u, ...action.user} : u;
  })),
);
