import { createReducer, on } from '@ngrx/store';
import * as UserAction from '../actions/user.actions';
import { User } from '../../model/user';

export const initialState: User = {};

export const reducer = createReducer(
  initialState,
  on(UserAction.setActive, (state, action) => ({ ...action.user}) )
);


