import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import { User } from '../../model/user';
import { reducer as usersReducer } from './users.reducer';
import { reducer as userReducer } from './user.reducer';
import { AppState } from '../../../../core/store/reducers';

export interface UsersState {
  list: User[];
  active: User;
}
export const reducers: ActionReducerMap<UsersState> = {
  list: usersReducer,
  active: userReducer,
};

export const getUsers = (state: any) => {
  console.log(state);
  return state
}
export const getFeaturesUsers = createFeatureSelector('users')

export const getUsersList = createSelector(
  getFeaturesUsers,
  (state: UsersState) => state.list
);

export const getUserActive = createSelector(
  getFeaturesUsers,
  (state: UsersState) => state.active
);

