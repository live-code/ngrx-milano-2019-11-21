import { createAction, props } from '@ngrx/store';
import { User } from '../../model/user';


export const loadUsers = createAction(
  '[users] load'
);
export const loadUsersSuccess = createAction(
  '[users] load success',
  props<{ users: User[]}>()
);
export const loadUsersFailed = createAction(
  '[users] load failed',
);


export const saveUser = createAction(
  '[users] save',
  props<{ user: User}>()
);

export const editUser = createAction(
  '[users] edit',
  props<{ user: User}>()
);
export const editUserSuccess = createAction(
  '[users] edit success',
  props<{ user: User}>()
);
export const editUserFailed = createAction(
  '[users] edit failed',
);


export const addUser = createAction(
  '[Users] Add',
  props<{ user: User}>()
);
export const addUserSuccess = createAction(
  '[Users] Add Success',
  props<{ user: User}>()
);
export const addUserFailed = createAction(
  '[Users] Add Failed'
);

export const deleteUser = createAction(
  '[Users] Delete',
  props<{ id: number}>()
);
export const deleteUserSuccess = createAction(
  '[Users] Delete Success',
  props<{ id: number}>()
);
export const deleteUserFailed = createAction(
  '[Users] Delete Failed'
);
