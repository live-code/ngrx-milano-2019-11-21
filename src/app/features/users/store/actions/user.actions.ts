import { createAction, props } from '@ngrx/store';
import { User } from '../../model/user';

export const setActive = createAction(
  '[user] set active',
  props<{ user: User}>()
);

export const cleanActive = createAction(
  '[user] clean',
);
