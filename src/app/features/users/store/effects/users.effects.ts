import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as UsersActions from '../actions/users.actions';
import { catchError, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';
import { of } from 'rxjs';
import { UsersService } from '../../services/users.service';

export class UsersEffects {

  loadUsers$ = createEffect(() => this.actions$.pipe(
    ofType(UsersActions.loadUsers),
    switchMap(
      () => this.userService.loadUsers()
        .pipe(
          map(users => UsersActions.loadUsersSuccess({ users })),
          catchError(() => of(UsersActions.loadUsersFailed()))
        )
    )
  ));

  loadUsersFailed$ = createEffect(
    () => this.actions$.pipe(
      ofType(UsersActions.loadUsersFailed),
      tap(() => console.log('toast'))
    ), { dispatch: false}
  );


  deleteUsers$ = createEffect(() => this.actions$.pipe(
    ofType(UsersActions.deleteUser),
    mergeMap(
      ({id}) => this.userService.deleteUser(id)
        .pipe(
          map(() => UsersActions.deleteUserSuccess({ id })),
          catchError(() => of(UsersActions.deleteUserFailed()))
        )
    )
  ));

  saveUser$ = createEffect(() => this.actions$.pipe(
    ofType(UsersActions.saveUser),
    mergeMap(({user}) => {
      if (user.id) {
        return of(UsersActions.editUser({ user }));
      } else {
        return of(UsersActions.addUser({ user }));
      }
    })
  ))

  addUser$ = createEffect(() => this.actions$.pipe(
    ofType(UsersActions.addUser),
    mergeMap(action =>
      this.userService.addUser(action.user)
        .pipe(
          map(data => UsersActions.addUserSuccess({ user: data})),
          catchError(() => of(UsersActions.addUserFailed())
          )
        )
    )
  ));

  editUsers$ = createEffect(() => this.actions$.pipe(
    ofType(UsersActions.editUser),
    mergeMap(action =>
      this.userService.editUser(action.user)
        .pipe(
          // If successful, dispatch success action with result
          map(data => UsersActions.editUserSuccess({ user: data})),
          // If request fails, dispatch failed action
          catchError(() => of(UsersActions.editUserFailed())
          )
        )
    )
  ));

  constructor(
    private actions$: Actions,
    private userService: UsersService,
  ) {}
}
