import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { AppState } from '../../../core/store/reducers';
import * as UsersActions from '../store/actions/users.actions';
import * as UserActions from '../store/actions/user.actions';
import { Observable } from 'rxjs';
import { User } from '../model/user';
import { getUserActive, getUsersList } from '../store/reducers';

@Component({
  selector: 'app-users-page',
  template: `
    
    <form #f="ngForm" (submit)="saveHandler(f.value)">
      <input 
        type="text"
        class="form-control"
        [ngModel]="(active$ | async).name"
        name="name"
      >
      <input 
        type="text"
        class="form-control"
        name="email"
      >
      
      <button
        class="btn btn-primary"
        type="submit" [disabled]="f.invalid">
        SAVE
      </button>
    </form>
    
    <hr>
    
    <li 
      class="list-group-item"
      *ngFor="let user of users$ | async"
      (click)="setActive(user)"
    >
      {{user.name}} - {{user.id}}
      
      <i class="fa fa-trash" (click)="deleteHandler(user, $event)"></i>
    </li>
    
    <pre>{{active$ | async | json}}</pre>
  `,
  styles: []
})
export class UsersPageComponent {
  users$: Observable<User[]> = this.store.pipe(select(getUsersList));
  active$: Observable<User> = this.store.pipe(select(getUserActive));

  constructor(private store: Store<any>) {
    store.dispatch(UsersActions.loadUsers());
  }

  deleteHandler(user: User, event: MouseEvent) {
    event.stopPropagation();
    const { id } = user;
    this.store.dispatch(UsersActions.deleteUser({ id }));
  }

  saveHandler(formData: any) {
    const user: User = formData as User;

    this.store.dispatch(UsersActions.saveUser({ user }));
  }

  setActive(user: User) {
   this.store.dispatch(UserActions.setActive({ user }));
  }
}
