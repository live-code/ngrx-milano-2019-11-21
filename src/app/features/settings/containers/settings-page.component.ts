import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { updateTheme } from '../../../core/store/actions/settings.actions';
import { Observable } from 'rxjs';
import { SettingsState } from '../../../core/store/reducers/settings.reducer';
import { selectTheme } from '../../../core/store/selectors/settings.selectors';
import { AppState } from '../../../core/store/reducers';

@Component({
  selector: 'app-settings-page',
  template: `
    
    <button
      class="btn btn-primary"
      [ngClass]="{'bg-warning': (theme$ | async) === 'light'}"
      (click)="updateThemeHandler('light')">light</button>
    <button
      class="btn btn-primary"
      [ngClass]="{'bg-warning': (theme$ | async) === 'dark'}"
      (click)="updateThemeHandler('dark')">dark</button>
  `,
  styles: []
})
export class SettingsPageComponent {
  theme$: Observable<string> = this.store.pipe(select(selectTheme));

  constructor(private store: Store<AppState>) {}

  updateThemeHandler(theme: string) {
    this.store.dispatch(updateTheme({ theme }));
  }
}
